ARG ARCH=amd64
ARG ALPINE_VERSION=3.10
FROM $ARCH/alpine:$ALPINE_VERSION

ARG APACHE_VERSION=2.4.41

EXPOSE 80/tcp

COPY src /
RUN apache-setup

WORKDIR /var/www/html
CMD ["apache-foreground"]
